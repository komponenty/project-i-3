package com.projekt.oknoprofesora;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.projekt.Ioknoprofesora.Ioknoprofesora;
import com.projekt.Person.IPerson;

public class oknoProfesora implements Ioknoprofesora, ActionListener {
	private JPanel oknoProfesora = new JPanel();
	int ID;
	JPanel pomoc ;
	JPanel administracja;
	JPanel przedmioty;
	JPanel ogloszenia;
	JMenuBar bar = new JMenuBar();
	//deklaracje mozliwych akcji
	//Program ->Pomoc
JMenu menuProgram=new JMenu("Program");
	JMenuItem Pomoc = new JMenuItem("Pomoc");
	
	
	//Administracja
	//Administracja uzytkownikami
	//Administracja Ocenami
	JMenu menuAdministracja = new JMenu("Administracja");
	JMenuItem adminOc = new JMenuItem("Administracja Przedmiotami");

	
	//Ogloszenia
	//Pokaz Ogloszenia
	JMenu menuOgloszenia= new JMenu("Ogloszenia");
	JMenuItem pogl= new JMenuItem("Pokaz Ogloszenia");
	public void initialize()
	{
		pomoc = Activator.pomoc.getHelpPanel();
		administracja = Activator.administracjaOsobami.getAdministracjaOsobamiPanel();
		przedmioty = Activator.przedmioty.getAdministracjaPrzedmiotamiPanel(ID, 3);
		ogloszenia=Activator.ogloszenia.getOgloszenia(3);
		//oknoAdmina.add(pomoc,BorderLayout.CENTER);
		//oknoAdmina.add(administracja,BorderLayout.CENTER);
		//pomoc.setVisible(false);
		//administracja.setVisible(false);
	}
	public oknoProfesora()
	{
		Pomoc.addActionListener(this);
		menuProgram.add(Pomoc);
		
		adminOc.addActionListener(this);
		menuAdministracja.add(adminOc);
		
		pogl.addActionListener(this);
		menuOgloszenia.add(pogl);
		
		bar.add(menuProgram);
		bar.add(menuAdministracja);
		bar.add(menuOgloszenia);
		
		
		oknoProfesora.setLayout(new BorderLayout());
		oknoProfesora.add(bar,BorderLayout.NORTH);
	}
	
	public void resizeParent()
	{
		Component c = oknoProfesora.getParent();  
        while( c.getParent() != null )  
        {  
            c = c.getParent();  
        }  
        Frame topFrame = ( Frame )c;
        topFrame.pack();
	}
	
	
	@Override
	public JPanel getOknoProfesora(int ID) {
		// TODO Auto-generated method stub
		IPerson osoba = Activator.baza.getSingleUser(ID);
		//	IPerson[] a = Activator.baza.getPerson();
		//	for(int i = 0;i<a.length;i++)
		//	{
		//				System.out.println(a[i].getImie());
		//	}

			JLabel ZalogowanyJako = new JLabel();
			ZalogowanyJako.setText("Zalogowany Jako: "+osoba.getImie()+" "+osoba.getNazwisko());
			oknoProfesora.add(ZalogowanyJako,BorderLayout.SOUTH);
			System.out.println(osoba.getImie());
			this.ID=ID;
		return oknoProfesora;
	}
	//TO JEST DO ZWRACANIA ODPOWIEDNIEGO JPANELA (jak wybierzemy np z menu "pomoc" to mamy tutaj case'a "pomoc"
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		String command = arg0.getActionCommand();
		switch(command)
{
		case "Pomoc":		
			Activator.pomoc.setHelpText("to jest text modułu pomocy");

			//administracja.setVisible(false);

			initialize();
			oknoProfesora.remove(administracja);
			oknoProfesora.remove(przedmioty);
			oknoProfesora.remove(ogloszenia);
			oknoProfesora.add(pomoc,BorderLayout.CENTER);
			//pomoc.setVisible(true);
			resizeParent();
			oknoProfesora.updateUI();
			break;
		case "Administracja Użytkownikami":
			//Activator.pomoc.setHelpText("@gi text pomocy");
			//oknoProfesora.updateUI();
		

			//pomoc.setVisible(false);
			
			initialize();
			oknoProfesora.remove(pomoc);
			oknoProfesora.remove(przedmioty);
			oknoProfesora.remove(ogloszenia);
			oknoProfesora.add(administracja,BorderLayout.CENTER);
			resizeParent();
			oknoProfesora.updateUI();
			break;
		case "Administracja Ocenami":
			initialize();
			oknoProfesora.remove(pomoc);
			oknoProfesora.remove(administracja);
			oknoProfesora.remove(ogloszenia);
			oknoProfesora.add(przedmioty,BorderLayout.CENTER);
			resizeParent();
			oknoProfesora.updateUI();
			//Activator.pomoc.setHelpText("dfgdfgd");
			//oknoProfesora.updateUI();
			break;
		case "Pokaz Ogloszenia":
			initialize();
			oknoProfesora.remove(pomoc);
			oknoProfesora.remove(administracja);
			oknoProfesora.remove(przedmioty);
			oknoProfesora.add(ogloszenia,BorderLayout.CENTER);
			resizeParent();
			oknoProfesora.updateUI();
			break;
		default:
				break;
		}
	}

}
