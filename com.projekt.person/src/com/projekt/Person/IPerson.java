package com.projekt.Person;

import com.projekt.persoImpl.Person;

public interface IPerson {

	public Person personFactory(int id,String Login,String Haslo, String n, String nazw, int Stopien);
	public void setImie(String imie);
	public String getImie();
	public int getID();
	public String getNazwisko();
	public void setNazwisko(String nazwisko);
	public int getStopien();
	public void setStopien(int Stopien);
	public void setLogin(String Login);
	public String getLogin();
	public String getHaslo();
	public void setHaslo(String Haslo);
	
}
