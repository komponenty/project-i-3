package com.projekt.persoImpl;

import com.projekt.Person.IPerson;



public class Person implements IPerson
{
	

	
	public Person(int id,String Login,String Haslo, String n, String nazw, int Stopien)
	{
		this.Login=Login;
		this.Haslo=Haslo;
		this.ID=id;
		Imie=n;
		Nazwisko=nazw;
		this.Stopien=Stopien;
	}
	public String Haslo;
	public String Login;
	 public String Nazwisko;
	 public int Stopien;	//0 - admin 1- wykladowca 2- student
	public int ID;
	public String Imie;
 
	@Override
	public void setImie(String imie) {
		this.Imie=imie;
		
	}
	@Override
	public String getImie() {
		// TODO Auto-generated method stub
		return Imie;
	}
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

	@Override
	public Person personFactory(int id,String Login,String Haslo, String n, String nazw, int Stopien) {
		// TODO Auto-generated method stub
		Person personToReturn = new Person(id,Login,Haslo,n,nazw,Stopien);
		return personToReturn;
	}
	@Override
	public String getNazwisko() {
		// TODO Auto-generated method stub
		return Nazwisko;
	}
	@Override
	public void setNazwisko(String nazwisko) {
		// TODO Auto-generated method stub
		this.Nazwisko=nazwisko;
		
	}
	@Override
	public int getStopien() {
		// TODO Auto-generated method stub
		return Stopien;
	}
	@Override
	public void setStopien(int Stopien) {
		// TODO Auto-generated method stub
		this.Stopien=Stopien;
	}
	@Override
	public void setLogin(String Login) {
		// TODO Auto-generated method stub
		this.Login=Login;
	}
	@Override
	public String getLogin() {
		// TODO Auto-generated method stub
		return Login;
	}
	@Override
	public String getHaslo() {
		// TODO Auto-generated method stub
		return Haslo;
	}
	@Override
	public void setHaslo(String Haslo) {
		this.Haslo=Haslo;
		
	}

}
