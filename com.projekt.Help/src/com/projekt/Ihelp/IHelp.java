package com.projekt.Ihelp;

import javax.swing.JPanel;

public interface IHelp {

	public void setHelpText(String text);
	public JPanel getHelpPanel();
}
