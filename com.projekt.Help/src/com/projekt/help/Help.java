package com.projekt.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.projekt.Ihelp.IHelp;

public class Help implements IHelp {

	public Help()
	{
		

		resultArea.setEditable(false);
        JScrollPane scrollingArea = new JScrollPane(resultArea);
        helpPanel.setLayout(new BorderLayout());
    helpPanel.add(scrollingArea);
	}
	JPanel helpPanel = new JPanel();
	JEditorPane resultArea = new JEditorPane();

	@Override
	public void setHelpText(String text) {
		// TODO Auto-generated method stub
		resultArea.setText(text);
		
	}

	@Override
	public JPanel getHelpPanel() {
		// TODO Auto-generated method stub
		helpPanel.setSize(500, 500);

		return helpPanel;
	}

}
