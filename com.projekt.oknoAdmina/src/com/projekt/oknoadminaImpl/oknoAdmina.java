package com.projekt.oknoadminaImpl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.projekt.Person.IPerson;
import com.projekt.oknoadmina.IoknoAdmina;

public class oknoAdmina implements IoknoAdmina, ActionListener {
	
	int ID;
	JPanel pomoc ;
	JPanel administracja;
	JPanel przedmioty;
	JPanel ogloszenia;
		private JPanel oknoAdmina = new JPanel();
	JMenuBar bar = new JMenuBar();
	//deklaracje mozliwych akcji
	//Program ->Pomoc
JMenu menuProgram=new JMenu("Program");
	JMenuItem Pomoc = new JMenuItem("Pomoc");
	
	
	//Administracja
	//Administracja uzytkownikami
	//Administracja Ocenami
	JMenu menuAdministracja = new JMenu("Administracja");
	JMenuItem adminUs= new JMenuItem("Administracja Użytkownikami");
	JMenuItem adminOc = new JMenuItem("Administracja Ocenami");
	
	//Ogloszenia
	//Pokaz Ogloszenia
	JMenu menuOgloszenia= new JMenu("Ogloszenia");
	JMenuItem pogl= new JMenuItem("Pokaz Ogloszenia");
	
	
	
	public oknoAdmina()
	{

		
		Pomoc.addActionListener(this);
		menuProgram.add(Pomoc);
		
		adminUs.addActionListener(this);
		menuAdministracja.add(adminUs);
		adminOc.addActionListener(this);
		menuAdministracja.add(adminOc);
		
		pogl.addActionListener(this);
		menuOgloszenia.add(pogl);
		
		bar.add(menuProgram);
		bar.add(menuAdministracja);
		bar.add(menuOgloszenia);
		
		
		
		oknoAdmina.setLayout(new BorderLayout());
		oknoAdmina.add(bar,BorderLayout.NORTH);
		
		
	}

	


	@Override
	public JPanel getOknoAdmina(int ID) {
		// TODO Auto-generated method stub
		IPerson osoba = Activator.baza.getSingleUser(ID);
	//	IPerson[] a = Activator.baza.getPerson();
	//	for(int i = 0;i<a.length;i++)
	//	{
	//				System.out.println(a[i].getImie());
	//	}

		this.ID=ID;
		JLabel ZalogowanyJako = new JLabel();
		ZalogowanyJako.setText("Zalogowany Jako: "+osoba.getImie()+" "+osoba.getNazwisko());
		oknoAdmina.add(ZalogowanyJako,BorderLayout.SOUTH);
		System.out.println(osoba.getImie());

		return oknoAdmina;
	}

	//redundancja kodu jak ...ekhm ale nie wiem jak to inaczej upchac : P 
	public void resizeParent()
	{
		Component c = oknoAdmina.getParent();  
        while( c.getParent() != null )  
        {  
            c = c.getParent();  
        }  
        Frame topFrame = ( Frame )c;
        topFrame.pack();
	}
	public void initialize()
	{
		pomoc = Activator.pomoc.getHelpPanel();
		administracja = Activator.administracjaOsobami.getAdministracjaOsobamiPanel();
		przedmioty = Activator.przedmioty.getAdministracjaPrzedmiotamiPanel(ID, 3);
		ogloszenia=Activator.ogloszenia.getOgloszenia(3);
		//oknoAdmina.add(pomoc,BorderLayout.CENTER);
		//oknoAdmina.add(administracja,BorderLayout.CENTER);
		//pomoc.setVisible(false);
		//administracja.setVisible(false);
	}
	public void setVisible()
	{
		
	}
	//TO JEST DO ZWRACANIA ODPOWIEDNIEGO JPANELA (jak wybierzemy np z menu "pomoc" to mamy tutaj case'a "pomoc"
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

		String command = arg0.getActionCommand();
		switch(command)
		{
		case "Pomoc":		
			Activator.pomoc.setHelpText("to jest text pomocy");

			//administracja.setVisible(false);

			initialize();
			oknoAdmina.remove(administracja);
			oknoAdmina.remove(przedmioty);
			oknoAdmina.remove(ogloszenia);
			oknoAdmina.add(pomoc,BorderLayout.CENTER);
			//pomoc.setVisible(true);
			resizeParent();
			oknoAdmina.updateUI();
			break;
		case "Administracja Użytkownikami":
			//Activator.pomoc.setHelpText("@gi text pomocy");
			//oknoAdmina.updateUI();
		

			//pomoc.setVisible(false);
			
			initialize();
			oknoAdmina.remove(pomoc);
			oknoAdmina.remove(przedmioty);
			oknoAdmina.remove(ogloszenia);
			oknoAdmina.add(administracja,BorderLayout.CENTER);
			resizeParent();
			oknoAdmina.updateUI();
			break;
		case "Administracja Ocenami":
			initialize();
			oknoAdmina.remove(pomoc);
			oknoAdmina.remove(administracja);
			oknoAdmina.remove(ogloszenia);
			oknoAdmina.add(przedmioty,BorderLayout.CENTER);
			resizeParent();
			oknoAdmina.updateUI();
			//Activator.pomoc.setHelpText("dfgdfgd");
			//oknoAdmina.updateUI();
			break;
		case "Pokaz Ogloszenia":
			initialize();
			oknoAdmina.remove(pomoc);
			oknoAdmina.remove(administracja);
			oknoAdmina.remove(przedmioty);
			oknoAdmina.add(ogloszenia,BorderLayout.CENTER);
			resizeParent();
			oknoAdmina.updateUI();
			break;
		default:
				break;
		}
		
	}






}
