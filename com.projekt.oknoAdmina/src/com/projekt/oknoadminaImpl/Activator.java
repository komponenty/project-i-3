package com.projekt.oknoadminaImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.IOgloszenia.IOgloszenia;
import com.projekt.Iadministracjaosobami.IAdministracjaOsobami;
import com.projekt.Ihelp.IHelp;
import com.projekt.administracjaprzedmiotami.IAdministracjaPrzedmiotami;
import com.projekt.baza.IBazaUserow;
import com.projekt.oknoadmina.IoknoAdmina;

public class Activator implements BundleActivator {

	public static IBazaUserow baza;
	public static IHelp pomoc;
	public static IAdministracjaPrzedmiotami przedmioty;
	public static IAdministracjaOsobami administracjaOsobami;
	public static IOgloszenia ogloszenia;
	private static BundleContext context;
	

	static BundleContext getContext() {
		return context;
	}


	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		context.registerService(IoknoAdmina.class.getName(), new oknoAdmina(), null);
		ServiceReference<IBazaUserow> ref1 = bundleContext.getServiceReference(IBazaUserow.class);
		baza=(IBazaUserow)bundleContext.getService(ref1);
		ServiceReference<IHelp> ref2=bundleContext.getServiceReference(IHelp.class);
		pomoc=(IHelp)bundleContext.getService(ref2);
		ServiceReference<IAdministracjaOsobami> ref3 = bundleContext.getServiceReference(IAdministracjaOsobami.class);
		administracjaOsobami=(IAdministracjaOsobami)bundleContext.getService(ref3);
		ServiceReference<IAdministracjaPrzedmiotami> ref4 = bundleContext.getServiceReference(IAdministracjaPrzedmiotami.class);
		przedmioty=(IAdministracjaPrzedmiotami)bundleContext.getService(ref4);
		ServiceReference<IOgloszenia> ref5 = bundleContext.getServiceReference(IOgloszenia.class);
		ogloszenia=(IOgloszenia)bundleContext.getService(ref5);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
