package com.projekt.showprzedmioty;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.Ishowprzedmioty.IprzedmiotyPanel;
import com.projekt.bazaprzedmiotow.IBazaPrzedmiotow;
import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class Activator implements BundleActivator {
	public static IBazaPrzedmiotow bazaprzedmiotow;
	public static IPrzedmiot przedmiot;
	public static IOceny oceny;
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		bundleContext.registerService(IprzedmiotyPanel.class.getName(), new przedmiotyPanel(), null);
		ServiceReference<IBazaPrzedmiotow> ref3 = bundleContext.getServiceReference(IBazaPrzedmiotow.class);
		bazaprzedmiotow=(IBazaPrzedmiotow)bundleContext.getService(ref3);
		ServiceReference<IPrzedmiot> ref4 = bundleContext.getServiceReference(IPrzedmiot.class);
		przedmiot=(IPrzedmiot)bundleContext.getService(ref4);
		ServiceReference<IOceny> ref5 = bundleContext.getServiceReference(IOceny.class);
		oceny=(IOceny)bundleContext.getService(ref5);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
