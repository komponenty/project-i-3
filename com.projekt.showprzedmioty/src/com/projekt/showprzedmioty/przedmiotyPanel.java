package com.projekt.showprzedmioty;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.projekt.Ishowprzedmioty.IprzedmiotyPanel;
import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class przedmiotyPanel implements IprzedmiotyPanel {

	int id;
	IPrzedmiot[] przedmioty;
	int idUsera;
	
	private JLabel[] nazwa = new JLabel[200];
	private JLabel[] ocena1= new JLabel[200];
	private JLabel[] ocena11= new JLabel[200];
	private JLabel[] ocena22= new JLabel[200];
	private JLabel[] ocena33 = new JLabel[200];
	public przedmiotyPanel()
	{
		
	}
	GridBagConstraints c = new GridBagConstraints();
	JPanel przedmiotyPanel = new JPanel();
	
	public void initialize()
	{
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=0;
		JLabel nazwap = new JLabel("Nazwa Przedmiotu");
		przedmiotyPanel.add(nazwap,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=0;
		JLabel ocenap = new JLabel("Ocena 1");
		przedmiotyPanel.add(ocenap,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=0;
		JLabel ocena2 = new JLabel("Ocena 2");
		przedmiotyPanel.add(ocena2,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=0;
		JLabel ocena3 = new JLabel("Ocena 3");
		przedmiotyPanel.add(ocena3,c);
		
		
		for(int i= 0; i<przedmioty.length;i++)
		{
			IOceny[] oceny = przedmioty[i].getListaOcen();
			int[] ocenyZPrzedmiotu = new int[3];
			for(int u=0;u<oceny.length;u++)
			{
				if(oceny[u].getIdOsoby()==idUsera)
				{
					int ocenyTmp[]=oceny[u].getOceny();
					try
					{
						ocenyZPrzedmiotu[0]=ocenyTmp[0];
						ocenyZPrzedmiotu[1]=ocenyTmp[1];
						ocenyZPrzedmiotu[2]=ocenyTmp[2];
					}
					catch(Exception exc)
					{
						
					}
					
				}
			}
			int x =0;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+1;
			nazwa[i]= new JLabel(przedmioty[i].getNazwa());
			przedmiotyPanel.add(nazwa[i],c);
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+1;
			ocena1[i]= new JLabel(String.valueOf(ocenyZPrzedmiotu[0]));
			przedmiotyPanel.add(ocena1[i],c);
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+1;
			ocena11[i]= new JLabel(String.valueOf(ocenyZPrzedmiotu[1]));
			przedmiotyPanel.add(ocena11[i],c);
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+1;
			ocena33[i]= new JLabel(String.valueOf(ocenyZPrzedmiotu[2]));
			przedmiotyPanel.add(ocena33[i],c);
			x++;
			

		}
		

	}
	
	int z =0;
	public JPanel getPrzedmiotyPanel(int IdUsera)
	{
		if(z==0)
		{
		Activator.bazaprzedmiotow.AddPrzedmiot(2, "przedmiot");
		IPrzedmiot[] b =Activator.bazaprzedmiotow.getPrzedmiotyAll();
		try
		{
			for(int u=0; u<b.length;u++)
			{
				b[u].addStudent(idUsera);
			}
		}
		catch(Exception ex)
		{
			
		}
		}
		z++;
		przedmiotyPanel.removeAll();
		this.idUsera=IdUsera;
		przedmioty=Activator.bazaprzedmiotow.getListaPrzedmiotowStudenta(IdUsera);
		przedmiotyPanel.setLayout(new GridBagLayout());
		initialize();
		return przedmiotyPanel;
	}
}
