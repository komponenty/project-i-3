package com.projekt.oknostudenta;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.IOgloszenia.IOgloszenia;
import com.projekt.Ihelp.IHelp;
import com.projekt.Ioknostudenta.Ioknostudenta;
import com.projekt.Ishowprzedmioty.IprzedmiotyPanel;
import com.projekt.baza.IBazaUserow;

public class Activator implements BundleActivator {

	private static BundleContext context;
	public static IHelp pomoc;
	public static IprzedmiotyPanel przedmiotyPanel;
	public static IBazaUserow baza;
	public static IOgloszenia ogloszenia;
	
	
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		
		context.registerService(Ioknostudenta.class.getName(), new oknoStudenta(), null);
		ServiceReference<IBazaUserow> ref1 = bundleContext.getServiceReference(IBazaUserow.class);
		baza=(IBazaUserow)bundleContext.getService(ref1);
		ServiceReference<IHelp> ref2=bundleContext.getServiceReference(IHelp.class);
		pomoc=(IHelp)bundleContext.getService(ref2);
		ServiceReference<IprzedmiotyPanel> ref3=bundleContext.getServiceReference(IprzedmiotyPanel.class);
		przedmiotyPanel=(IprzedmiotyPanel)bundleContext.getService(ref3);
		ServiceReference<IOgloszenia> ref4=bundleContext.getServiceReference(IOgloszenia.class);
		ogloszenia=(IOgloszenia)bundleContext.getService(ref4);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
