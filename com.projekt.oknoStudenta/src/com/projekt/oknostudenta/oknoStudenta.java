package com.projekt.oknostudenta;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.projekt.Ioknostudenta.Ioknostudenta;
import com.projekt.Person.IPerson;

public class oknoStudenta implements Ioknostudenta, ActionListener {

	JPanel pomoc;
	JPanel ogloszenie;
	JPanel oceny;
	int IdUsera;
	private JPanel oknoStudenta = new JPanel();
	JMenuBar bar = new JMenuBar();
	//deklaracje mozliwych akcji
	//Program ->Pomoc
JMenu menuProgram=new JMenu("Program");
	JMenuItem Pomoc = new JMenuItem("Pomoc");
	
	
	//Administracja
	//Administracja uzytkownikami
	//Administracja Ocenami
	JMenu menuAdministracja = new JMenu("Oceny");
	JMenuItem adminOc = new JMenuItem("Pokaz Oceny");

	
	//Ogloszenia
	//Pokaz Ogloszenia
	JMenu menuOgloszenia= new JMenu("Ogloszenia");
	JMenuItem pogl= new JMenuItem("Pokaz Ogloszenia");
	
	public oknoStudenta()
	{

		Pomoc.addActionListener(this);
		menuProgram.add(Pomoc);
		
		adminOc.addActionListener(this);
		menuAdministracja.add(adminOc);
		
		pogl.addActionListener(this);
		menuOgloszenia.add(pogl);
		
		bar.add(menuProgram);
		bar.add(menuAdministracja);
		bar.add(menuOgloszenia);
		
		
		oknoStudenta.setLayout(new BorderLayout());
		oknoStudenta.add(bar,BorderLayout.NORTH);
	}
	
	public void resizeParent()
	{
		Component c = oknoStudenta.getParent();  
        while( c.getParent() != null )  
        {  
            c = c.getParent();  
        }  
        Frame topFrame = ( Frame )c;
        topFrame.pack();
	}
	
	public void startuj()
	{
		pomoc=Activator.pomoc.getHelpPanel();
		oceny=Activator.przedmiotyPanel.getPrzedmiotyPanel(IdUsera);
		ogloszenie=Activator.ogloszenia.getOgloszenia(1);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String command = arg0.getActionCommand();
		switch(command)
		{
		case "Pomoc":	
			startuj();
			Activator.pomoc.setHelpText("to jest text pomocy");
			oknoStudenta.remove(ogloszenie);
			oknoStudenta.remove(oceny);
			oknoStudenta.add(pomoc,BorderLayout.CENTER);
			resizeParent();
			oknoStudenta.updateUI();
			break;
		case "Pokaz Oceny":
			startuj();
			oknoStudenta.remove(ogloszenie);
			oknoStudenta.remove(pomoc);
			oknoStudenta.add(oceny,BorderLayout.CENTER);
			resizeParent();
			oknoStudenta.updateUI();
			break;
		case "Pokaz Ogloszenia":
			Activator.ogloszenia.setOgloszenie("Brak Ogloszen!");
			startuj();
			oknoStudenta.remove(oceny);
			oknoStudenta.remove(pomoc);
			oknoStudenta.add(ogloszenie,BorderLayout.CENTER);
			resizeParent();
			oknoStudenta.updateUI();
			break;
		default:
				break;
		}
		
		
	}

	@Override
	public JPanel getOknoStudenta(int ID) {
		IPerson osoba = Activator.baza.getSingleUser(ID);
		//	IPerson[] a = Activator.baza.getPerson();
		//	for(int i = 0;i<a.length;i++)
		//	{
		//				System.out.println(a[i].getImie());
		//	}
this.IdUsera=ID;
			JLabel ZalogowanyJako = new JLabel();
			ZalogowanyJako.setText("Zalogowany Jako: "+osoba.getImie()+" "+osoba.getNazwisko());
			oknoStudenta.add(ZalogowanyJako,BorderLayout.SOUTH);
			System.out.println(osoba.getImie());
		return oknoStudenta;
	}

}
