package com.projekt.exeptionhandlerImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.projekt.exceptionhandler.IExceptionHandler;


public class Activator implements BundleActivator {
     private BundleContext m_context = null;

    public void start(BundleContext context) throws Exception {
         m_context = context;
    
    m_context.registerService(IExceptionHandler.class.getName(), new ExceptionHandler(), null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
