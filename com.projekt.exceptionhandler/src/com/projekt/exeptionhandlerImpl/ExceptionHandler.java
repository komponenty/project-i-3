/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projekt.exeptionhandlerImpl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

import com.projekt.exceptionhandler.IExceptionHandler;

/**
 *
 * @author Adaś
 */
public class ExceptionHandler implements IExceptionHandler {

    final private String file = "logs.xml";
    private List<String> Errors;
    private LogFrame frame;
    public ExceptionHandler() {
         Errors = new ArrayList<String>();
         frame = new LogFrame();
    }

    
    
    public List<String> getErrors()  {
               try{
                    FileInputStream fis = new FileInputStream(file); 
                    ObjectInputStream ois = new ObjectInputStream(fis); 
                    this.Errors = (List<String>)ois.readObject(); 
                    ois.close(); 
               }catch(Exception ex){ this.Errors=null; }
                
        return this.Errors;
    }

    public void setErrors(List<String> Errors) {
        try{
        FileOutputStream fstream = new FileOutputStream(file); 
        ObjectOutputStream stream = new ObjectOutputStream(fstream); 
        stream.writeObject(this.getErrors()); 
        stream.flush(); 
        stream.close();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    public void makeLog(String Exception) {
        String error = new Date().toString()+";"+Exception;
        this.getErrors().add(error);
        this.setErrors(this.getErrors());
    }

    public void getLogs(String[] Logs) {
       frame.setjTable1(this.getErrors());
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               frame.setVisible(true);
            }
        });
    }
    
}
