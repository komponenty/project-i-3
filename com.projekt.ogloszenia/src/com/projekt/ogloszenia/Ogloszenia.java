package com.projekt.ogloszenia;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import com.projekt.IOgloszenia.IOgloszenia;

public class Ogloszenia implements IOgloszenia {

	public Ogloszenia()
	{
		
	}
	JPanel ogloszenia = new JPanel();
	JTextArea textOgloszen = new JTextArea("Tutaj mozemy zamieszczac przykladowe ogloszenia",30,30);
	
	public JPanel getOgloszenia(int Stopien)
	{
		if(Stopien==1)
		{
			textOgloszen.setEditable(false);
		}
		else
		{
			textOgloszen.setEditable(true);
		}
	   ogloszenia.add(textOgloszen);
	   return ogloszenia;
	}

	public void setOgloszenie(String ogloszenie) {
		// TODO Auto-generated method stub
		textOgloszen.setText(ogloszenie);		
	}
}
