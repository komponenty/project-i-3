package com.projekt.przedmiotImpl;



import java.util.LinkedList;
import java.util.List;

import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class Przedmiot implements IPrzedmiot {

	private static int IDs;
	private List<IOceny> Listaocen= new LinkedList<IOceny>();
	int IdProwadzacego;
	int IdPrzedmiotu;
	String Nazwa;
	
	
	public Przedmiot(int idProwadzacego, int IdPrzedmiotu, String Nazwa)
	{
		this.IdProwadzacego=idProwadzacego;
		this.IdPrzedmiotu=IdPrzedmiotu;
		this.Nazwa=Nazwa;
	}
	
	public void EditPrzedmiot(int IdStudenta, int[] Oceny)
	{
		for(int i=0;i<Listaocen.size();i++)
		{
			if(IdStudenta==Listaocen.get(i).getId())
			{
				Listaocen.get(i).setOceny(Oceny);
			}
		}
	}
	
	//wypluwa liste ocen dla daego przedmiotu
	public IOceny[] getListaOcen()
	{
		return Listaocen.toArray(new IOceny[Listaocen.size()]);
	}
	
	public IPrzedmiot getPrzedmiot()
	{
		return this;
	}
	
	public int getIdProwadzacego()
	{
		return IdProwadzacego;
	}
	public int getIdPzedmiotu()
	{
		return IdPrzedmiotu;
	}
	//dodawanie nowych kursantow
	public void addStudent(int IdStudenta)
	{
		int[] array= new int[3];
		array[0]=0;
		array[1]=0;
		array[2]=0;
		IOceny tmp = new Oceny(IDs,IdStudenta,array);
		IDs++;
		Listaocen.add(tmp);
	}
	public void EditStudent(int IdOceny,int[] array, int IdStudenta)
	{
		for(int i =0;i<Listaocen.size();i++)
		{
			if(Listaocen.get(i).getId()==IdOceny)
			{
				Listaocen.get(i).setOceny(array);
			}
		}
	}
	
	//usuwanie kursantow
	public void deleteStudent(int IdStudenta)
	{
		for(int i=0;i<Listaocen.size();i++)
		{
			IOceny tmp = Listaocen.get(i);
			if(tmp.getEntity(IdStudenta)!=null)
			{
				Listaocen.remove(i);
			}
		}
	}

	@Override
	public IPrzedmiot przdmiotFactory(int IdProwadzacego,int IdPrzedmiotu,String Nazwa) {
		// TODO Auto-generated method stub
		Przedmiot tmp = new Przedmiot(IdProwadzacego,IdPrzedmiotu, Nazwa);
		return tmp;
	}
	@Override
	public String getNazwa() {
		// TODO Auto-generated method stub
		return Nazwa;
	}
	@Override
	public void setIdProwadzacego(int Id) {
		// TODO Auto-generated method stub
		this.IdPrzedmiotu=Id;
		
	}
	@Override
	public void setNazwa(String Nazwa) {
		// TODO Auto-generated method stub
		this.Nazwa=Nazwa;
		
	}
	
}



//klasa do przechowywania ocen
