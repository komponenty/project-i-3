package com.projekt.przedmiotImpl;

import com.projekt.przedmiot.IOceny;

public class Oceny implements IOceny
{
	int[] oceny;
	int Idosoby;
	int Id;
	public Oceny(int ID,int idOsoby, int[] oceny)
	{
		this.Id=ID;
		this.oceny=oceny;
		this.Idosoby=idOsoby;
	}
	public IOceny getEntity(int idOsoby)
	{
		if(this.Idosoby==idOsoby)
		{
			return this;
		}
		else
		{
			return null;
		}
	}
	public int getId()
	{
		return this.Id;
	}
	
	public int[] getOceny(){
		return oceny;
	}
	public void setOceny(int[] oceny)
	{
		this.oceny=oceny;
	
	}
	public int getIdOsoby() {
		// TODO Auto-generated method stub
		return this.Idosoby;
	}
}
