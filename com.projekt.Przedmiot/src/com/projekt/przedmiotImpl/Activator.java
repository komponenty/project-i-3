package com.projekt.przedmiotImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		int[] tb = new int[3];
		bundleContext.registerService(IOceny.class.getName(), new Oceny(0,0,tb), null);
		bundleContext.registerService(IPrzedmiot.class.getName(), new Przedmiot(0,0,"test"), null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
