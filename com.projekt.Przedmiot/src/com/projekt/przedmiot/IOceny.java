package com.projekt.przedmiot;

public interface IOceny {

	public IOceny getEntity(int idOsoby);
	public int getIdOsoby();
	public int getId();
	public int[] getOceny();
	public void setOceny(int[] oceny);
}
