package com.projekt.przedmiot;


public interface IPrzedmiot {

	//wypluwa liste ocen dla daego przedmiotu
		public IOceny[] getListaOcen();
		
		public IPrzedmiot getPrzedmiot();
		
		public int getIdProwadzacego();
		public int getIdPzedmiotu();
		//dodawanie nowych kursantow
		public void addStudent(int IdStudenta);
		

		public void EditStudent(int IdOceny,int[] array, int IdStudenta);
		//usuwanie kursantow
		public void deleteStudent(int IdStudenta);
		
		public IPrzedmiot przdmiotFactory(int IdProwadzacego, int IdPrzedmiotu,String Nazwa);
		public void EditPrzedmiot(int IdStudenta, int[] Oceny);
		public String getNazwa();
		public void setIdProwadzacego(int Id);
		public void setNazwa(String Nazwa);
}
