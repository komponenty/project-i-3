package com.projekt.main;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.Imain.Imain;
import com.projekt.logowanie.ILoguj;

//import com.projekt.logowanie.ILoguj;

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		context.registerService(Imain.class.getName(), new Main(), null);
		ServiceReference<ILoguj> ref = bundleContext.getServiceReference(ILoguj.class);
		Main ma = new Main();
		
	    
	    ma.Start(bundleContext.getService(ref).getPanel());
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
