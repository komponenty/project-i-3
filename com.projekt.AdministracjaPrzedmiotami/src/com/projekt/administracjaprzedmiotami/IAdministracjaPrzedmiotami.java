package com.projekt.administracjaprzedmiotami;

import javax.swing.JPanel;

public interface IAdministracjaPrzedmiotami {

	public JPanel getAdministracjaPrzedmiotamiPanel(int IdUsera, int Stopien);
}
