package com.projekt.administracjaprzedmiotamiImpl;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicComboBoxRenderer;


import com.projekt.Person.IPerson;
import com.projekt.administracjaprzedmiotami.IAdministracjaPrzedmiotami;
import com.projekt.baza.IBazaUserow;
import com.projekt.bazaprzedmiotow.IBazaPrzedmiotow;
import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class AdministracjaPrzedmiotami implements IAdministracjaPrzedmiotami, ActionListener {

	
	int IdUsera;
	int Stopien;
	final JFrame f = new JFrame("Detale");
	Details dt;
	JPanel SubjectAdminPanel= new JPanel();
	public static JPanel panelToReturn = new JPanel();
	GridBagConstraints c = new GridBagConstraints();
	// d = new GridBagConstraints();
	private final JButton[] Editbuttons = new JButton[200];
	private final JButton[] Deletebuttons= new JButton[200];
	private final JLabel[] PrzedmiotIDlabel= new JLabel[200];
	public final static JTextField[] Nazwafields = new JTextField[200];
	private final JTextField[] fields= new JTextField[200];
	private final JTextField[] Loginfields = new JTextField[200];
	private final JTextField[] Stopienfields = new JTextField[200];
	private final JButton[] DetailsButtons = new JButton[200];
	private JScrollPane scrollPanel;
	public static IPrzedmiot[] przedmioty;
	JComboBox comboBox;
	IBazaUserow baza;
	IBazaPrzedmiotow bazaPrzedmiotow;
	int IdPrzedm;
	int i;
	private void initialize()
	{ try
	{
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=0;
		JLabel ImieTytul2 = new JLabel("Prowadzacy");
		SubjectAdminPanel.add(ImieTytul2,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=0;
		JLabel Akcja11 = new JLabel("Nazwa Przedmiotu");
		SubjectAdminPanel.add(Akcja11,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=0;
		JLabel Akcja12 = new JLabel("Akcja");
		SubjectAdminPanel.add(Akcja12,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=0;
		JLabel Akcja13 = new JLabel("");
		SubjectAdminPanel.add(Akcja13,c);
		
		if(Stopien==3)
		{
			Vector model = new Vector();
			IPerson[] a =Activator.baza.getWykladowcy();
			for(int i =0;i<a.length;i++)
			{
				model.addElement(new Item(a[i].getID(),a[i].getLogin()));
			}
			comboBox = new JComboBox(model);
	        
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=0;
			c.gridy=1;
			SubjectAdminPanel.add(comboBox,c);
	        
		}
		else
		{
	        
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=0;
			c.gridy=1;
			JLabel IdWywol = new JLabel(String.valueOf(IdUsera));
			SubjectAdminPanel.add(IdWywol,c);
		}
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=1;
		final JTextField NazwaTextF = new JTextField(10);
		SubjectAdminPanel.add(NazwaTextF, c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=1;
		JButton AddButton = new JButton("Dodaj Przedmiot");
		
		AddButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				try
				{
					if(Stopien==3)
					{
						Item a = (Item)comboBox.getSelectedItem();
				Activator.bazaprzedmiotow.AddPrzedmiot(a.getId(), NazwaTextF.getText());
				AdministracjaPrzedmiotami.przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyAll();
				initialize();
					}
					else
					{
						Activator.bazaprzedmiotow.AddPrzedmiot(IdUsera, NazwaTextF.getText());
						AdministracjaPrzedmiotami.przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyProwadzacego(IdUsera);
						initialize();
					}

				}
				catch(Exception exc)
				{
					
				}
				
			}
		
		});
		AddButton.addActionListener(this);
		AddButton.addActionListener(this);
		SubjectAdminPanel.add(AddButton, c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=1;
		JLabel labelz = new JLabel("");
		SubjectAdminPanel.add(labelz, c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=2;
		JLabel ImieTytul1 = new JLabel("Nazwa");
		SubjectAdminPanel.add(ImieTytul1,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=2;
		JLabel Akcja = new JLabel("Akcja");
		SubjectAdminPanel.add(Akcja,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=2;
		JLabel Akcja1 = new JLabel("");
		SubjectAdminPanel.add(Akcja1,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=2;
		JLabel Akcja2 = new JLabel("");
		SubjectAdminPanel.add(Akcja2,c);
		
		for(i = 0; i< przedmioty.length;i++)
		{
			IdPrzedm =przedmioty[i].getIdPzedmiotu();
			int x = 0;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Nazwafields[i] = new JTextField(przedmioty[i].getNazwa());
			SubjectAdminPanel.add(Nazwafields[i],c);
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Editbuttons[i] = new JButton("Zapisz");
			Editbuttons[i].addActionListener(new MyEditActionListener(przedmioty[i].getIdPzedmiotu(),i));
			//Editbuttons[i].addActionListener(new MyEditActionListener(przedmioty[i].getIdPzedmiotu(),Nazwafields[i].getText()));
			//Editbuttons[i].addActionListener(this);
			//Editbuttons[i].addActionListener(this);
			SubjectAdminPanel.add(Editbuttons[i],c);
			
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Deletebuttons[i] = new JButton("Usu�");
			Deletebuttons[i].addActionListener(new MyDeleteActionListener(przedmioty[i].getIdPzedmiotu()));
			Deletebuttons[i].addActionListener(new MyDeleteActionListener(przedmioty[i].getIdPzedmiotu()));
			Deletebuttons[i].addActionListener(this);
			Deletebuttons[i].addActionListener(this);
			SubjectAdminPanel.add(Deletebuttons[i],c);
			
			x++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			DetailsButtons[i] = new JButton("Szczeg�ly");
			DetailsButtons[i].addActionListener(new ActionListener(){
					
				
				Details dtz = new Details();
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub

					System.out.print(dtz);
					//JFrame f = new JFrame("detale");
					
					System.out.print(f);
					//f.removeAll();
					
							//f.getContentPane().add(dt.getFrame(IdPrzedm));

					
						//f.getContentPane().add(dtz.getFrame(IdPrzedm).updateUI());

					//System.out.print(f);
					//f.pack();
					//f.setVisible(true);
					dtz.getFrame(IdPrzedm).setVisible(true);
					
					
				}});
			//Deletebuttons[i].addActionListener(new MyDeleteActionListener(przedmioty[i].getIdPzedmiotu()));
			//Deletebuttons[i].addActionListener(new MyDeleteActionListener(przedmioty[i].getIdPzedmiotu()));
			//Deletebuttons[i].addActionListener(this);
			//Deletebuttons[i].addActionListener(this);
			SubjectAdminPanel.add(DetailsButtons[i],c);
			
			
			
			
		}
	}
	catch(Exception e)
	{
		
	}
	}
	
	
	
			int z =0;
	@Override
	public JPanel getAdministracjaPrzedmiotamiPanel(int IdUsera, int Stopien) {
		// TODO Auto-generated method stub

		if(z==0)
		{
			Activator.bazaprzedmiotow.AddPrzedmiot(0, "Przedmiot 1");
			Activator.bazaprzedmiotow.AddPrzedmiot(0, "Przedmiot 2");
			Activator.baza.AddUser("wykladowca1", "haslo", "Jarek", "jarkowski", 2);
			Activator.baza.AddUser("wykladowca2", "haslo", "Jarek", "jarkowski", 2);
		
			this.IdUsera=IdUsera;
			this.Stopien=Stopien;
			if(Stopien ==3)
			{
				przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyAll();
			}
			if(Stopien==2)
			{
				przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyProwadzacego(IdUsera);
			}
			SubjectAdminPanel.setLayout(new GridBagLayout());
			initialize();
			scrollPanel = new JScrollPane(SubjectAdminPanel);
			panelToReturn.add(scrollPanel);
		}
		z++;
		return panelToReturn;
	}




	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		SubjectAdminPanel.removeAll();
		SubjectAdminPanel.removeAll();
		if(Stopien ==3)
		{
			przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyAll();
		}
		if(Stopien==2)
		{
			przedmioty=Activator.bazaprzedmiotow.getPrzedmiotyProwadzacego(IdUsera);
		}
		//initialize();
		initialize();
		AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToReturn.updateUI();
		JPanel pn = (JPanel)panelToReturn.getParent();
		pn.updateUI();
		AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToReturn.updateUI();
		JPanel pnz = (JPanel)panelToReturn.getParent();
		pnz.updateUI();
	}

}

class MyEditActionListener implements ActionListener
{
	int id;
	int i;
	String Nazwa;
	public MyEditActionListener(int text,int i)
	{
		this.i=i;
		this.id=text;

		//this.i=i;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		this.Nazwa=AdministracjaPrzedmiotami.Nazwafields[i].getText();
		Activator.bazaprzedmiotow.EditPrzedmiot(id, Nazwa);
		//Activator.baza.EditUser(id,login, haslo, imie, nazwisko, Stopien);
		//AdministracjaOsobami.panelToReturn.updateUI();
		//IPerson p = Activator.baza.getSingleUser(id);
		//System.out.print(p.getNazwisko());
		
		//AdministracjaOsobami.personTable=Activator.baza.getPerson();
	  
		
	}
	
}

class MyDeleteActionListener implements ActionListener
{
	
int id;
public MyDeleteActionListener(int text)
{
	this.id=text;
}

@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	//System.out.print(Activator.baza.getSingleUser(id));
	Activator.bazaprzedmiotow.RemovePrzedmiot(id);
	//AdministracjaOsobami.panelToReturn.updateUI();
	//AdministracjaPrzedmiotami.=Activator.baza.getPerson();
	//System.out.print(Activator.baza.getSingleUser(id));
	
}
	
}





//---DO COMBO BOXA

class ItemRenderer extends BasicComboBoxRenderer
{
    public Component getListCellRendererComponent(
        JList list, Object value, int index,
        boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index,
            isSelected, cellHasFocus);

        if (value != null)
        {
            Item item = (Item)value;
            setText( item.getDescription().toUpperCase() );
        }

        if (index == -1)
        {
            Item item = (Item)value;
            setText( "" + item.getId() );
        }


        return this;
    }
}

class Item
{
    private int id;
    private String description;

    public Item(int id, String description)
    {
        this.id = id;
        this.description = description;
    }

    public int getId()
    {
        return id;
    }

    public String getDescription()
    {
        return description;
    }

    public String toString()
    {
        return description;
    }
}




//--Frame do detali (wstawianie ocen itd)

class Details implements ActionListener
{
	public final static JButton[] Editbuttons = new JButton[200];
	public final static JButton[] Deletebuttons= new JButton[200];
	public final static JTextField[] NameFields= new JTextField[200];
	public final static JTextField[] Ocena1fields = new JTextField[200];
	public final static JTextField[] Ocena2fields= new JTextField[200];
	public final static JTextField[] Loginfields = new JTextField[200];
	public final static JTextField[] Ocena3fields = new JTextField[200];
	public final static JButton[] DetailsButtons = new JButton[200];
	JPanel panelToShow=new JPanel();
	public IPrzedmiot przedmiot;
	IOceny[] oceny;
	IPerson[] a;
	IPerson osoba;
	JComboBox comboBox;
	private JFrame panel=new JFrame();
	private JScrollPane scrollPanel;
	GridBagConstraints c = new GridBagConstraints();
	public int IdPrzedmiotu;
	int i;
	Vector model;
	public void initialize()
	{

		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=0;
		JLabel user = new JLabel("Login");
		panelToShow.add(user,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=0;
		JLabel akcja1 = new JLabel("Akcja");
		panelToShow.add(akcja1,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=0;

		JLabel akcja11 = new JLabel("");
		panelToShow.add(akcja11,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=0;

		JLabel akcja112 = new JLabel("");
		panelToShow.add(akcja112,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=4;
		c.gridy=0;

		JLabel akcja1111 = new JLabel("");
		panelToShow.add(akcja1111,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=5;
		c.gridy=0;

		JLabel akcja111 = new JLabel("");
		panelToShow.add(akcja111,c);
		
		
		
		//Combo box!
	 model = new Vector();
	 a =Activator.baza.getStudenci();
		for(int i =0;i<a.length;i++)
		{
			model.addElement(new Item(a[i].getID(),a[i].getLogin()));
			System.out.println(a[1].getID());
			System.out.println(a[i].getLogin());
		}
		comboBox = new JComboBox(model);
        
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=1;
		panelToShow.add(comboBox,c);
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=1;
		JButton buttonDodaj = new JButton("Dodaj");
		buttonDodaj.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				IPrzedmiot pr = Activator.bazaprzedmiotow.getSinglePrzedmiot(IdPrzedmiotu);
				Item sel = (Item)comboBox.getSelectedItem();
				pr.addStudent(sel.getId());
				oceny=przedmiot.getListaOcen();
				initialize();
			//	System.out.println(sel.getId());
				
			}});
		buttonDodaj.addActionListener(this);
		buttonDodaj.addActionListener(this);
		panelToShow.add(buttonDodaj,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=1;
		JLabel buttonDodaj1 = new JLabel("");
		panelToShow.add(buttonDodaj1,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=1;
		JLabel buttonDodaj11 = new JLabel("");
		panelToShow.add(buttonDodaj11,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=4;
		c.gridy=1;
		JLabel buttonDodaj12 = new JLabel("");
		panelToShow.add(buttonDodaj12,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=5;
		c.gridy=1;
		JLabel buttonDodaj13 = new JLabel("");
		panelToShow.add(buttonDodaj13,c);
		
		
		
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=2;
		JLabel NazwaLabel = new JLabel("Login");
		panelToShow.add(NazwaLabel,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=1;
		c.gridy=2;
		JLabel Ocena1 = new JLabel("Ocena 1");
		panelToShow.add(Ocena1,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=2;
		c.gridy=2;
		JLabel Ocena2 = new JLabel("Ocena 2");
		panelToShow.add(Ocena2,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=3;
		c.gridy=2;
		JLabel Ocena3 = new JLabel("Ocena 3");
		panelToShow.add(Ocena3,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=4;
		c.gridy=2;
		JLabel Edytuj = new JLabel("Akcje");
		panelToShow.add(Edytuj,c);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx=5;
		c.gridy=2;
		JLabel Ede = new JLabel("");
		panelToShow.add(Ede,c);
		
		
		
		
		
		
		for(i=0;i<oceny.length;i++)
		{
			int x = 0;
			int[] useraOceny = oceny[i].getOceny();
			osoba=Activator.baza.getSingleUser(oceny[i].getIdOsoby());
			
			
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			NameFields[i] = new JTextField(osoba.getLogin());
			NameFields[i].setEditable(false);
			panelToShow.add(NameFields[i],c);
			
			
			x++;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Ocena1fields[i] = new JTextField(String.valueOf(useraOceny[0]),10);
			panelToShow.add(Ocena1fields[i],c);
			
			x++;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Ocena2fields[i] = new JTextField(String.valueOf(useraOceny[1]));
			panelToShow.add(Ocena2fields[i],c);
			
			x++;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Ocena3fields[i] = new JTextField(String.valueOf(useraOceny[2]));
			panelToShow.add(Ocena3fields[i],c);
			
			x++;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Editbuttons[i] = new JButton("Zapisz");
			Editbuttons[i].addActionListener(new MyOcenyEditActionListener(oceny[i].getId(), oceny[i].getIdOsoby(), IdPrzedmiotu, i));
			//Editbuttons[i].addActionListener(this);
			panelToShow.add(Editbuttons[i],c);
			
			x++;
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=x;
			c.gridy=i+3;
			Deletebuttons[i] = new JButton("Usu�");
			Deletebuttons[i].addActionListener(new MyOcenyDeleteActionListener(osoba.getID(),IdPrzedmiotu));
			Deletebuttons[i].addActionListener(this);
			panelToShow.add(Deletebuttons[i],c);
			
			
		}
		
	}
	int z = 0;
	public JFrame getFrame(int IdPrzedmiotu)
	{

		panel.getContentPane().removeAll();

		panelToShow.removeAll();
		IPrzedmiot a = Activator.bazaprzedmiotow.getSinglePrzedmiot(IdPrzedmiotu);
	//	if(z==0)
	//	{
	//a.addStudent(0);
	//	System.out.println(a.getIdPzedmiotu());
	//	System.out.println(a.getIdProwadzacego());
		//}
		z++;
		przedmiot=Activator.bazaprzedmiotow.getSinglePrzedmiot(IdPrzedmiotu);
		oceny=przedmiot.getListaOcen();
		panelToShow.setLayout(new GridBagLayout());
		initialize();
		System.out.println(panelToShow);
		scrollPanel = new JScrollPane(panelToShow);
			panel.getContentPane().add(scrollPanel);
		panel.pack();
		this.IdPrzedmiotu=IdPrzedmiotu;
		panel.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		return panel;
	}
	
	
	public void Action()
	{
		panelToShow.removeAll();
		panelToShow.removeAll();
		przedmiot=Activator.bazaprzedmiotow.getSinglePrzedmiot(IdPrzedmiotu);
		oceny=przedmiot.getListaOcen();
		//initialize();
		initialize();
		//AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToShow.updateUI();
		//JPanel pn = (JPanel)panelToReturn.getParent();
		//pn.updateUI();
		//AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		panelToShow.removeAll();
		panelToShow.removeAll();
		przedmiot=Activator.bazaprzedmiotow.getSinglePrzedmiot(IdPrzedmiotu);
		oceny=przedmiot.getListaOcen();
		//initialize();
		initialize();
		//AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToShow.updateUI();
		//JPanel pn = (JPanel)panelToReturn.getParent();
		//pn.updateUI();
		//AdministracjaPrzedmiotami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		//panelToReturn.updateUI();
		//JPanel pnz = (JPanel)panelToReturn.getParent();
		//pnz.updateUI();
		
	}

}

class MyOcenyDeleteActionListener implements ActionListener
{

	int id;
	int idPrzedmiotu;
	public MyOcenyDeleteActionListener(int ID,int IdPrzedmiotu)
	{
		this.id=ID;
		this.idPrzedmiotu=IdPrzedmiotu;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		IPrzedmiot a = Activator.bazaprzedmiotow.getSinglePrzedmiot(idPrzedmiotu);
		a.deleteStudent(id);
	
		
	}
	
}

class MyOcenyEditActionListener implements ActionListener
{
	int i;
	int Id;
	int IdOceny;
	int idPrzedmiotu;
	int Ocena1;
	int Ocena2;
	int Ocena3;
	int[] Oceny = new int[3];
	public MyOcenyEditActionListener(int IdOceny,int Id, int IdPrzedmiotu, int i)
	{
		this.i=i;
		this.Id=Id;
		this.IdOceny=IdOceny;
		this.idPrzedmiotu=IdPrzedmiotu;
		//this.Ocena1=Integer.parseInt(Ocena1);
		//this.Ocena2=Ocena2;
		///this.Ocena3=Ocena3;

		//Oceny[0]=this.Ocena1;
		//Oceny[1]=Ocena2;
		//Oceny[2]=Ocena3;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Oceny[0]=Integer.parseInt(Details.Ocena1fields[i].getText());
		Oceny[1]=Integer.parseInt(Details.Ocena2fields[i].getText());
		Oceny[2]=Integer.parseInt(Details.Ocena3fields[i].getText());
				System.out.println(Ocena1);
		Activator.bazaprzedmiotow.EditPrzedmiotOceny(IdOceny,idPrzedmiotu, Id, Oceny);
		//System.out.println(Oceny[0]);
		//System.out.println(Id);
		//System.out.println(idPrzedmiotu);
		IPrzedmiot a =Activator.bazaprzedmiotow.getSinglePrzedmiot(idPrzedmiotu);
		a.EditStudent(IdOceny,Oceny, Id);
		
	}
	
}

