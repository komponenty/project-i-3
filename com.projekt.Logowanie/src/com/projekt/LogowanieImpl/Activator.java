package com.projekt.LogowanieImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.Ioknoprofesora.Ioknoprofesora;
import com.projekt.Ioknostudenta.Ioknostudenta;
import com.projekt.Person.IPerson;
import com.projekt.baza.IBazaUserow;
import com.projekt.logowanie.ILoguj;
import com.projekt.oknoadmina.IoknoAdmina;

public class Activator implements BundleActivator {

	private static BundleContext context;


	public static IBazaUserow baza;
	public static IoknoAdmina oknoAdmina;
	public static Ioknoprofesora oknoProfesora;
	public static Ioknostudenta oknoStudenta;
	
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		context.registerService(ILoguj.class.getName(), new Logowanie(), null);

		ServiceReference<IBazaUserow> ref1 = bundleContext.getServiceReference(IBazaUserow.class);
		ServiceReference<IoknoAdmina> ref2 = bundleContext.getServiceReference(IoknoAdmina.class);
		ServiceReference<Ioknoprofesora> ref3 = bundleContext.getServiceReference(Ioknoprofesora.class);
		ServiceReference<Ioknostudenta> ref4 = bundleContext.getServiceReference(Ioknostudenta.class);
		baza=(IBazaUserow)bundleContext.getService(ref1);
		oknoAdmina=(IoknoAdmina)bundleContext.getService(ref2);
		oknoProfesora=(Ioknoprofesora)bundleContext.getService(ref3);
		oknoStudenta=(Ioknostudenta)bundleContext.getService(ref4);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}
	public IPerson getPerson()
	{
		ServiceReference<IPerson> ref1 = getContext().getServiceReference(IPerson.class);
		return getContext().getService(ref1);
	}

}
