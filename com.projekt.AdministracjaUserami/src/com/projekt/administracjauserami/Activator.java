package com.projekt.administracjauserami;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.Iadministracjaosobami.IAdministracjaOsobami;
import com.projekt.Person.IPerson;
import com.projekt.baza.IBazaUserow;
import com.projekt.exceptionhandler.IExceptionHandler;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator implements BundleActivator {

	private static BundleContext context;
public static IPerson person;
public static IBazaUserow baza;
public static IExceptionHandler handler;
	static BundleContext getContext() {
		return context;
	}

	/*/
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		context.registerService(IAdministracjaOsobami.class.getName(), new AdministracjaOsobami(), null);
		ServiceReference<IPerson> ref1 = bundleContext.getServiceReference(IPerson.class);
		person=(IPerson)bundleContext.getService(ref1);
		ServiceReference<IBazaUserow> ref2 = bundleContext.getServiceReference(IBazaUserow.class);
		baza=(IBazaUserow)bundleContext.getService(ref2);
		
		ServiceReference<IExceptionHandler> ref3 = bundleContext.getServiceReference(IExceptionHandler.class);
		handler=(IExceptionHandler)bundleContext.getService(ref3);
	}
/*
 * 
 * 
 */
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}


}

