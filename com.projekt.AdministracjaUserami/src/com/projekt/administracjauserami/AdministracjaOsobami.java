package com.projekt.administracjauserami;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;


import com.projekt.Iadministracjaosobami.IAdministracjaOsobami;
import com.projekt.Person.IPerson;
import com.projekt.baza.IBazaUserow;

public class AdministracjaOsobami implements IAdministracjaOsobami, ActionListener {


	JPanel personAdminPanel= new JPanel();
	JPanel addPanel=new JPanel();
	public static JPanel panelToReturn = new JPanel();
	GridBagConstraints c = new GridBagConstraints();
	// d = new GridBagConstraints();
	private final JButton[] Editbuttons = new JButton[200];
	private final JButton[] Deletebuttons= new JButton[200];
	private final JLabel[] userIDlabel= new JLabel[200];
	public static final JTextField[] Imiefields = new JTextField[200];
	public static final JTextField[] Nazwisofields= new JTextField[200];
	public static final JTextField[] Loginfields = new JTextField[200];
	public static final JTextField[] Stopienfields = new JTextField[200];
	public static final JTextField[] Haslofields = new JTextField[200];
	private JScrollPane scrollPanel;
	//GridLayout layoutToReturn = new GridLayout(0,1);
	public static IPerson[] personTable;
	IBazaUserow baza;
	int i;
	private void initializeComponent()
	{
		
		try
		{
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=0;
			c.gridy=0;
			JLabel ImieTytul1 = new JLabel("Imie");
			personAdminPanel.add(ImieTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=1;
			c.gridy=0;
			JLabel NazwiskoTytul1= new JLabel("Nazwisko");
			personAdminPanel.add(NazwiskoTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=2;
			c.gridy=0;
			JLabel LoginTytul1  = new JLabel("Login");
			personAdminPanel.add(LoginTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=3;
			c.gridy=0;
			JLabel HasloTytul1  = new JLabel("Has�o");
			personAdminPanel.add(HasloTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=4;
			c.gridy=0;
			JLabel StopienTytul1 = new JLabel("Stopien (zakres 1-3)");
			personAdminPanel.add(StopienTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=5;
			c.gridy=0;
			JLabel AkcjeTytul1 = new JLabel("Dodaj");
			personAdminPanel.add(AkcjeTytul1,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=6;
			c.gridy=0;
			JLabel AkcjeTytul11 = new JLabel("");
			personAdminPanel.add(AkcjeTytul11,c);
			
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=7;
			c.gridy=0;
			JLabel AkcjeTytul12 = new JLabel("");
			personAdminPanel.add(AkcjeTytul12,c);
			
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 1;
			final JTextField userImieTxt = new JTextField("",10);
			personAdminPanel.add(userImieTxt,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = 1;
			final JTextField userNazwiskoTxt = new JTextField("",10);
			personAdminPanel.add(userNazwiskoTxt,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 2;
			c.gridy = 1;
			final JTextField userLoginTxt = new JTextField("",10);
			personAdminPanel.add(userLoginTxt,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 3;
			c.gridy = 1;
			final JTextField userHasloTxt = new JTextField("",10);
			personAdminPanel.add(userHasloTxt,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 4;
			c.gridy = 1;
			final JTextField userStopienTxt = new JTextField("",10);
			personAdminPanel.add(userStopienTxt,c);
			
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 5;
			c.gridy = 1;
			JButton dodajButtonz = new JButton("Dodaj U�ytkownika");
			//dodajButtonz.addActionListener(new MyAddActionListener(userImieTxt.getText(),userNazwiskoTxt.getText(),userLoginTxt.getText(),userHasloTxt.getText(),Integer.parseInt(userStopienTxt.getText())));
			//dodajButtonz.addActionListener(new MyAddActionListener(userImieTxt.getText(),userNazwiskoTxt.getText(),userLoginTxt.getText(),userHasloTxt.getText(),Integer.parseInt(userStopienTxt.getText())));
			dodajButtonz.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					try
					{
					Activator.baza.AddUser(userLoginTxt.getText(), userHasloTxt.getText(), userImieTxt.getText(), userNazwiskoTxt.getText(),Integer.parseInt(userStopienTxt.getText()));
					AdministracjaOsobami.personTable=Activator.baza.getPerson();
					initializeComponent();
					}
					catch(Exception exc)
					{
						
					}
					
				}
			});
			dodajButtonz.addActionListener(this);
			dodajButtonz.addActionListener(this);

			
			personAdminPanel.add(dodajButtonz,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 6;
			c.gridy = 1;
			JLabel dodajButton1 = new JLabel(" ");
			personAdminPanel.add(dodajButton1,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 7;
			c.gridy = 1;
			JLabel dodajButton12 = new JLabel(" ");
			personAdminPanel.add(dodajButton12,c);
			
			
			
			

			
			

			
			//--------------------------------------------------//
			
			
			
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=0;
			c.gridy=2;
			JLabel IdTytul = new JLabel("ID");
			personAdminPanel.add(IdTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=1;
			c.gridy=2;
			JLabel ImieTytul = new JLabel("Imie");
			personAdminPanel.add(ImieTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=2;
			c.gridy=2;
			JLabel NazwiskoTytul= new JLabel("Nazwisko");
			personAdminPanel.add(NazwiskoTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=3;
			c.gridy=2;
			JLabel LoginTytul  = new JLabel("Login");
			personAdminPanel.add(LoginTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=4;
			c.gridy=2;
			JLabel HasloTytul  = new JLabel("Has�o");
			personAdminPanel.add(HasloTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=5;
			c.gridy=2;
			JLabel StopienTytul = new JLabel("Stopien (zakres 1-3)");
			personAdminPanel.add(StopienTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=6;
			c.gridy=2;
			JLabel AkcjeTytul = new JLabel("Akcje");
			personAdminPanel.add(AkcjeTytul,c);
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridx=7;
			c.gridy=2;
			JLabel AkcjeT = new JLabel(" ");
			personAdminPanel.add(AkcjeT,c);
			
			
			for(i = 0; i < personTable.length;i++)
			{
				int x =0;
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				userIDlabel[i]=new JLabel(String.valueOf(personTable[i].getID()));
				personAdminPanel.add(userIDlabel[i],c);
				
				x++;
				
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				Imiefields[i] = new JTextField(personTable[i].getImie(),10);
				personAdminPanel.add(Imiefields[i],c);
				
				x++;
				
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				Nazwisofields[i]= new JTextField(personTable[i].getNazwisko(),10);
				personAdminPanel.add(Nazwisofields[i],c);
				
				x++;
				
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				Loginfields[i]=new JTextField(personTable[i].getLogin(),10);
				personAdminPanel.add(Loginfields[i],c);
				
				x++;
				
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				Haslofields[i]=new JTextField(personTable[i].getHaslo(),10);
				personAdminPanel.add(Haslofields[i],c);
				
				x++;
				
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				Stopienfields[i]= new JTextField(String.valueOf(personTable[i].getStopien()),10);
				personAdminPanel.add(Stopienfields[i],c);
				
				x++;
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				//c.weightx=.5;
				Editbuttons[i]= new JButton("Zapisz zmiany");
				Editbuttons[i].addActionListener(new MyEditActionListener(Integer.parseInt(userIDlabel[i].getText()),i));
				//Editbuttons[i].addActionListener(this);
				//Editbuttons[i].addActionListener(new MyEditActionListener(Integer.parseInt(userIDlabel[i].getText()),personTable[i].getImie(),Nazwisofields[i].getText(),Loginfields[i].getText(),Haslofields[i].getText(),Integer.parseInt(Stopienfields[i].getText())));
				//Editbuttons[i].addActionListener(this);
				personAdminPanel.add(Editbuttons[i],c);
				
				x++;
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = i+3;
				//c.weightx=0.5;
				Deletebuttons[i] = new JButton("Usu�");
				Deletebuttons[i].addActionListener(new MyDeleteActionListener(personTable[i].getID()));
				Deletebuttons[i].addActionListener(this);
				Deletebuttons[i].addActionListener(new MyDeleteActionListener(personTable[i].getID()));
				Deletebuttons[i].addActionListener(this);
				personAdminPanel.add(Deletebuttons[i],c);
			
			
			}
		}
		catch(Exception e)
		{
			
		}
	}
	public AdministracjaOsobami()
	{
		
	}
	int z =0;
	public JPanel getAdministracjaOsobamiPanel() {
		// TODO Auto-generated method stub
		if(z==0)
		{
			//addPanel.setLayout(new GridBagLayout());
			personTable=  Activator.baza.getPerson();
			//System.out.print(personTable[0].getImie());
		personAdminPanel.setLayout(new GridBagLayout());
		initializeComponent();
		scrollPanel = new JScrollPane(personAdminPanel);
		//scrollPanel.setPreferredSize(new Dimension (personAdminPanel.getWidth(),200));
		panelToReturn.add(scrollPanel,BorderLayout.CENTER);
		//panelToReturn.add(addPanel,BorderLayout.NORTH);
		}
		z++;
		return panelToReturn;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		personAdminPanel.removeAll();
		personAdminPanel.removeAll();
		personTable=Activator.baza.getPerson();
		initializeComponent();
		initializeComponent();
		AdministracjaOsobami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToReturn.updateUI();
		JPanel pn = (JPanel)panelToReturn.getParent();
		pn.updateUI();
		AdministracjaOsobami.panelToReturn.updateUI();
		scrollPanel.updateUI();
		panelToReturn.updateUI();
		JPanel pnz = (JPanel)panelToReturn.getParent();
		pnz.updateUI();
		
		
	}
	
	

	
	
	
	
}


class MyEditActionListener implements ActionListener
{
	//public MyEditActionListener(int text, String Imie, String Nazwisko, String login, String haslo, int Stopien
	int id;
	int i;
	String imie;
	String nazwisko;
	String login;
	String haslo;
	int Stopien;
	public MyEditActionListener(int text, int i)
	{
		this.id=text;
		//this.imie=Imie;
		//this.nazwisko=Nazwisko;
		//this.login=login;
		//this.haslo=haslo;
		//this.Stopien=Stopien;
		//this.i=i;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.imie=AdministracjaOsobami.Imiefields[i].getText();
		this.nazwisko=AdministracjaOsobami.Nazwisofields[i].getText();
		this.login=AdministracjaOsobami.Loginfields[i].getText();
		this.haslo=AdministracjaOsobami.Haslofields[i].getText();
		try
		{
		this.Stopien=Integer.parseInt(AdministracjaOsobami.Stopienfields[i].getText());
		}
		catch(Exception excp)
		{
			
		}
		Activator.baza.EditUser(id,login, haslo, imie, nazwisko, Stopien);
		AdministracjaOsobami.panelToReturn.updateUI();
		IPerson p = Activator.baza.getSingleUser(id);
		System.out.print(p.getNazwisko());
		
		AdministracjaOsobami.personTable=Activator.baza.getPerson();
	  
		
	}
	
}

class MyDeleteActionListener implements ActionListener
{
	
int id;
public MyDeleteActionListener(int text)
{
	this.id=text;
}

@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	//System.out.print(Activator.baza.getSingleUser(id));
	Activator.baza.RemoveUser(id);
	//AdministracjaOsobami.panelToReturn.updateUI();
	AdministracjaOsobami.personTable=Activator.baza.getPerson();
	//System.out.print(Activator.baza.getSingleUser(id));
	
}
	
}
class MyAddActionListener implements ActionListener
{
	String imie;
	String nazwisko;
	String login;
	String haslo;
	int Stopien;
	public MyAddActionListener(String Imie, String Nazwisko, String login, String haslo, int Stopien)
	{
		this.imie=Imie;
		this.nazwisko=Nazwisko;
		this.login=login;
		this.haslo=haslo;
		this.Stopien=Stopien;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		Activator.baza.AddUser(login, haslo, imie, nazwisko, Stopien);
		AdministracjaOsobami.personTable=Activator.baza.getPerson();
		
	}
}

