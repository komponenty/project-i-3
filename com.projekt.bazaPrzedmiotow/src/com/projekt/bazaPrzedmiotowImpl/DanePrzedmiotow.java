package com.projekt.bazaPrzedmiotowImpl;


import java.util.LinkedList;
import java.util.List;

import com.projekt.bazaprzedmiotow.IBazaPrzedmiotow;
import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class DanePrzedmiotow implements IBazaPrzedmiotow {

	public DanePrzedmiotow()
	{
		
	}
	
	
	private static int IDgen;
	
	private List<IPrzedmiot> ListaPrzedmiotow = new LinkedList<IPrzedmiot>();
	
	
	public IPrzedmiot[] getListaPrzedmiotowStudenta(int idStudenta)
	{
		List<IPrzedmiot> przedmiotToReturn = new LinkedList<IPrzedmiot>();
		for(int a = 0; a<ListaPrzedmiotow.size();a++)
		{
			IPrzedmiot przedmiot= ListaPrzedmiotow.get(a);
			IOceny[] oceny = przedmiot.getListaOcen();
			for(int z = 0 ; z<oceny.length;z++)
			{
				if(oceny[z].getIdOsoby()==idStudenta)
				{
					przedmiotToReturn.add(przedmiot);
					break;
				}
			}
			
		}
		if(idStudenta==1)
		{
			
		}
		IPrzedmiot[] przedmiotArray = przedmiotToReturn.toArray(new IPrzedmiot[przedmiotToReturn.size()]);
		return przedmiotArray;
	}
	
	public void AddPrzedmiot(int IdProwadzacego,String Nazwa)
	{
		IPrzedmiot p = Activator.przedmiot.przdmiotFactory(IdProwadzacego, IDgen,Nazwa);
		IDgen++;
		ListaPrzedmiotow.add(p);
	}
	public void AddOcene(int IdPrzedmiotu,IOceny oc)
	{
		
	}
	public void RemovePrzedmiot(int ID)
	{
		for(int i = 0; i<ListaPrzedmiotow.size();i++)
		{
			IPrzedmiot tmp = ListaPrzedmiotow.get(i);
			if(tmp.getIdPzedmiotu()==ID)
			{
				ListaPrzedmiotow.remove(i);
			}
		}
	}
	
	public void EditPrzedmiot(int IdPrzedmiotu, String Nazwa)
	{
		for(int i = 0; i<ListaPrzedmiotow.size();i++)
		{
			IPrzedmiot tmp = ListaPrzedmiotow.get(i);
			if(tmp.getIdPzedmiotu()==IdPrzedmiotu)
			{
			
				tmp.setNazwa(Nazwa);
				ListaPrzedmiotow.set(i, tmp);
			}
		}
	}
	
	public IPrzedmiot[] getPrzedmiotyAll()
	{
		IPrzedmiot[] przedmioty= ListaPrzedmiotow.toArray(new IPrzedmiot[ListaPrzedmiotow.size()]);
		return przedmioty;
	
	}
	
	public IPrzedmiot[] getPrzedmiotyProwadzacego(int IdProwadzacego)
	{
		List<IPrzedmiot> tmp = new LinkedList<IPrzedmiot>();
		
		for(int i = 0; i<ListaPrzedmiotow.size();i++)
		{
			IPrzedmiot tmp1 = ListaPrzedmiotow.get(i);
			if(tmp1.getIdProwadzacego()==IdProwadzacego)
			{
				tmp.add(tmp1);
			}
		}
		
		IPrzedmiot[] arrayToReturn= tmp.toArray(new IPrzedmiot[tmp.size()]);
		return arrayToReturn;
	}
	
	public void EditPrzedmiotOceny(int IdOceny,int IdPrzedmiotu,int IdUsera, int[] oceny)
	{
		//IPrzedmiot a;
		for(int i = 0; i < ListaPrzedmiotow.size();i++)
		{
			if(ListaPrzedmiotow.get(i).getIdPzedmiotu()==IdPrzedmiotu)
			{
				ListaPrzedmiotow.get(i).EditStudent(IdOceny,oceny, IdUsera);
				//Activator.przedmiot.EditPrzedmiot(IdUsera, oceny);
			}
		}
	}
	
	public IPrzedmiot getSinglePrzedmiot(int IdPrzedmiotu)
	{
		for(int i = 0; i < ListaPrzedmiotow.size();i++)
		{
			if(ListaPrzedmiotow.get(i).getIdPzedmiotu()==IdPrzedmiotu)
			{
				return ListaPrzedmiotow.get(i);
			}
		}
		return null;
	}
	
}
