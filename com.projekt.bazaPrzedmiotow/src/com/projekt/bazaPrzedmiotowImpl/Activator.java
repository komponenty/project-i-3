package com.projekt.bazaPrzedmiotowImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.projekt.bazaprzedmiotow.IBazaPrzedmiotow;
import com.projekt.przedmiot.IOceny;
import com.projekt.przedmiot.IPrzedmiot;

public class Activator implements BundleActivator {

	public static IOceny oceny;
	public static IPrzedmiot przedmiot;
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		ServiceReference<IOceny> ref1= bundleContext.getServiceReference(IOceny.class);
		ServiceReference<IPrzedmiot> ref2=bundleContext.getServiceReference(IPrzedmiot.class);
		oceny= bundleContext.getService(ref1);
		przedmiot=bundleContext.getService(ref2);
		bundleContext.registerService(IBazaPrzedmiotow.class.getName(), new DanePrzedmiotow(), null);
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
