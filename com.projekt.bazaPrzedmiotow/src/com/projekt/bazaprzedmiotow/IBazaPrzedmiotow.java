package com.projekt.bazaprzedmiotow;
import com.projekt.przedmiot.IPrzedmiot;

public interface IBazaPrzedmiotow {
	public void AddPrzedmiot(int IdProwadzacego,String Nazwa);
	
	public void RemovePrzedmiot(int ID);
	
	public void EditPrzedmiot(int IdPrzedmiotu, String Nazwa);
	
	public IPrzedmiot[] getPrzedmiotyAll();
	
	public IPrzedmiot[] getPrzedmiotyProwadzacego(int IdProwadzacego);
	
	public IPrzedmiot getSinglePrzedmiot(int IdPrzedmiotu);
	public void EditPrzedmiotOceny(int IdOceny,int IdPrzedmiotu,int IdUsera, int[] oceny);
	public IPrzedmiot[] getListaPrzedmiotowStudenta(int idStudenta);
}
