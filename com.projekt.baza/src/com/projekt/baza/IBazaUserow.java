package com.projekt.baza;

import com.projekt.Person.IPerson;
import com.projekt.bazaImpl.DaneUserow;




public interface IBazaUserow {

	public IPerson getSingleUser(int ID);
	public void AddUser(String Login,String Haslo,String n, String nazw, int Stopien);

	public void save(String path);
	public void load(String path);
	public void RemoveUser(int ID);

	public void EditUser(int ID,String Login,String Haslo,String n, String nazw, int Stopien);
public DaneUserow create();
	public IPerson[] getPerson();
	public String test();
	public IPerson[] getWykladowcy();
	public IPerson[] getStudenci();
}
