package com.projekt.bazaImpl;

import java.util.LinkedList;
import java.util.List;

import com.projekt.Person.IPerson;
import com.projekt.baza.IBazaUserow;

public class DaneUserow implements IBazaUserow {

	private static int IDGen;
	private static int ID=0;
	private List<IPerson> UsersInfo= new LinkedList<IPerson>();
	
	public DaneUserow()
	{
	}
	public void AddUser(String Login,String Haslo,String n, String nazw, int Stopien)
	{
		IPerson p = Activator.person.personFactory(IDGen,Login,Haslo, n, nazw, Stopien);
		IDGen++;
		UsersInfo.add(p);
	}
	public DaneUserow create()
	{
		DaneUserow a = new DaneUserow();
		return a;
	}
	public void RemoveUser(int ID)
	{
		for(int i = 0; i<UsersInfo.size();i++)
		{
			IPerson tmp = UsersInfo.get(i);
			if(tmp.getID()==ID)
			{
				UsersInfo.remove(i);
			}
		}
	}
	public void EditUser(int ID,String Login,String Haslo, String n, String nazw, int Stopien)
	{
		for(int i = 0; i<UsersInfo.size();i++)
		{
			IPerson tmp = UsersInfo.get(i);
			if(tmp.getID()==ID)
			{
				tmp.setHaslo(Haslo);
				tmp.setLogin(Login);
				tmp.setNazwisko(nazw);
				tmp.setImie(n);
				tmp.setStopien(Stopien);
				
				UsersInfo.set(i, tmp);
			}
		}
	}
	public IPerson[] getPerson()
	{
		return UsersInfo.toArray(new IPerson[UsersInfo.size()]);
	}
	@Override
	public String test() {
		// TODO Auto-generated method stub
		return Activator.person.getClass().toString();
	}
	@Override
	public IPerson getSingleUser(int ID) {
		// TODO Auto-generated method stub
		for(int i = 0;i<UsersInfo.size();i++)
		{
			IPerson tmp = UsersInfo.get(i);
			if(tmp.getID()==ID)
			{
				return tmp;
			}
		}
		return null;
	}
	@Override
	public void save(String path) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void load(String path) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public IPerson[] getWykladowcy() {
		// TODO Auto-generated method stub
		List<IPerson> listaWykladowcow = new LinkedList<IPerson>();
		for(int i = 0;i<UsersInfo.size();i++)
		{
			IPerson tmp = UsersInfo.get(i);
			if(tmp.getStopien()==2)
			{
				listaWykladowcow.add(tmp);
			}
		}
		IPerson[] toReturn = listaWykladowcow.toArray(new IPerson[listaWykladowcow.size()]);
		return toReturn;
	}
	
	public IPerson[] getStudenci() {
		// TODO Auto-generated method stub
		List<IPerson> listaWykladowcow = new LinkedList<IPerson>();
		for(int i = 0;i<UsersInfo.size();i++)
		{
			IPerson tmp = UsersInfo.get(i);
			if(tmp.getStopien()==1)
			{
				listaWykladowcow.add(tmp);
			//	System.out.println(tmp.getID());
			}
		}
		IPerson[] toReturn = listaWykladowcow.toArray(new IPerson[listaWykladowcow.size()]);
		for(int z = 0; z<toReturn.length;z++)
		{
			//System.out.println(toReturn[z].getID()+" d");
		}
		return toReturn;
	}
}

