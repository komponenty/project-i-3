package com.projekt.bazaImpl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;


import com.projekt.Person.IPerson;
import com.projekt.baza.IBazaUserow;


public class Activator implements BundleActivator {

	private static BundleContext context;
public static IPerson person;
	static BundleContext getContext() {
		return context;
	}

	/*/
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		context.registerService(IBazaUserow.class.getName(), new DaneUserow(), null);
		ServiceReference<IPerson> ref1 = bundleContext.getServiceReference(IPerson.class);
		person=(IPerson)bundleContext.getService(ref1);
	}
/*
 * 
 * 
 */
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}
	public static IPerson getPerson()
	{
		ServiceReference<IPerson> ref1 = getContext().getServiceReference(IPerson.class);
		return getContext().getService(ref1);
	}

}
